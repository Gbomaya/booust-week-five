<?php 

class ContactUs
{

    public static function process($param) {

        date_default_timezone_set("Africa/Lagos");

        // OPEN OR CREATE contact.csv IF NOT EXISTING
        $file = fopen('contact.csv', 'a');

        //CREATE CSV COLLUMNS (RUN THIS ONCE)
        fputcsv($file, array('Name', 'Email', 'Message','Date'));

        //APPEND CONTACT DETAILS TO CSV FILE
        fputcsv($file, array($param['name'], $param['email'], $param['message'], date( "Y-m-d h:i:sa")));

        //CLOSE FILE STREAM
        fclose($file);

        //PREPARE EMAIL MESSAGE AND SEND TO USER
        $message = "Hello ".$param['name'].",\n\n Thank you for reaching out.\n\nRegards." ;
        $headers = "Reply-To: Movie Admin <rental@movies.com>\r\n";
        $headers .= "Return-path: Movie Admin <rental@movies.com>\r\n";
        $headers .= "From: Movie Admin <rental@movies.com>\r\n";

        //SEND EMAIL
        mail($param['email'], "Booust Contact", $message, $headers);


        /*__________
         READ THE INFORMATION FROM THE CSV FILE
         _____________
        */
        $csv = array_map( "str_getcsv", file( 'contact.csv'));
        echo $csv[sizeof($csv)-1][0] . '<br>';
        echo $csv[sizeof($csv)-1][1] . '<br>';
        echo $csv[sizeof($csv)-1][2] . '<br>';
        echo $csv[sizeof($csv)-1][3] . '<br>';
}
}
?>
