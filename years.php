<?php 

/*
|_______________________________
|Three steps to find a leap year
1. Write a forloop that loops through the years 1980 to 2018
2. write a function that runs when an if statement is satisfied
Implement a counter that runs when a function is fulfilled
|_______________________________
*/

/* 
1. year % 4 ==0 --> Evenly divisible by 4
2. AND year % 100 !== 0 --> Should not be divisible by 100
3. OR year % 400 ==0 --> Evenly divisible by 400
*/

 
  
  $year = 1980;
  $counter = 0;

  for ($year = 1980; $year <= 2018; $year++) {
    if (isLeapYear($year)) {
      $counter = $counter + 1;
      echo $year . 'Leap Year' . '<br>';
   } else {
      echo $year . "<br>";
   }

  }
  
  # ECHO THE NUMBER OF LEAP YEARS
  echo '<br>' . 'Total number of leap years =' . $counter;
  
/*
|_______________________________
|FUNCTION TO DETERMINE LEAP YEAR
|_______________________________
*/
  
function isLeapYear($year) {

  $isLeapYear = false;
  if (($year % 4 == 0 ) && ( $year % 100 !== 0) || ($year % 400 == 0) ){
    
    $isLeapYear = true;
     return $isLeapYear;
  }
     
}

  
?>