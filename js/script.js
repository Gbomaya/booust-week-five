$(document).ready(function(){
    $(".slide1").click(function(){
        $(".open1").append("slow");
    });
    $(".slide2").click(function(){
        $(".open2").slideToggle("slow");
    });
    $(".slide3").click(function(){
        $(".open3").slideToggle("slow");
    });
});

$(document).ready(function(){
    $(".sky1").click(function(){
      $(this).fadeToggle("slow",0.4)
    });
});
$(document).ready(function(){
    $('[data-toggle="tooltip"]').tooltip();   
});


$(document).ready(function(){
    $("#submitForm").click(function(){
        var emailAddress = $("#email").val();
        var fullName = $("#fullName").val();
        var message = $("#comment").val();

        if (emailAddress ==="") {
            $("#email_error").text("Please enter your email Address");
        } else { 
            $("#email_error").text("");
        }
        if (fullName ==="") {
            $("#fullName_error").text("Please enter your Full Name");
        } else { 
            $("#fullName_error").text("");
        }
        if (message ==="") {
            $("#message_error").text("Please enter your message");
        } else { 
            $("#message_error").text("");
        }

     if (emailAddress!="" && fullName !="" && message !="") {
         var formValues = {
             emailAddress: emailAddress,
             fullName: fullName,
             message : message,
         };
        console.log(formValues);
     }
     return false;
});
});