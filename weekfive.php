<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>IKORODU Cinemas</title>
    <link href="https://fonts.googleapis.com/css?family=Merriweather|Ubuntu" rel="stylesheet">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="./bootstrap files/styles/bootstrap.min.css">
    <link rel="stylesheet" href="./css/index.css"> 
    <script src="js/jquery"></script>
    <script src="./bootstrap files/scripts/bootstrap.min.js"></script>
    <script src="./js/script.js"></script>
    
</head>
<body>

<div class="container-fluid">
<header>
    <div class=" pull-left">
                   <a href="/"><img src="./images/banner.jpg" alt="Wallion" title="Wallion" style="height: 62px; width: 150px;" ></a>
    </div>
    <nav class="navbar navbar-expand-sm nucolor navbar-dark navbar-fixed-top left">
    <a class="navbar-brand" href="weekfive.html">HOME</a>
     <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#collapsibleNavbar">
        <span class="navbar-toggler-icon"></span>
      </button>
    <div class="collapse navbar-collapse" id="collapsibleNavbar">
      <ul class="navbar-nav">
        <li class="nav-item">
        <a class="nav-link" href="about.html">ABOUT US</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="Contact.html">CONTACT US</a>
        </li>
       </ul>
    </div>  
    </nav>  
</header>           
   
<div class="jumbotron" style="background-image:url(images/missionimp2.jpg)">
        <div class="container" style="color: white">
        <h1>WELCOME TO IKORODU CINEMAS</h1>
        <p>WE SHOW YOUR FAVORITE MOVIES</p>
         <p><a href="about.html" class="btn btn-primary btn-lg middle text-center">EXPLORE OUR MOVIES</a></p>
      </div>
</div> 
  
<div id="nplaying"> <p href="#" data-toggle="tooltip" title="NOW PLAYING!" style="color:black;font-size: larger;" >Movies at IKC</p> </span></div>
<main class="container-fluid"> 
  <div class="row movies"  style="color: #24262a">
        <div class="col-xs-12 col-sm-4 well well-lg">
        <div class="thumbnail">
        <img src="./images/banner2.jpg" alt="first picture"  class=" sky1" style="width: 100%;height: 100%;">
        <div class="caption">
        <h3>SKYSCRAPER</h3>
        <p>Former  agent and amputee Will Sawyer lives in the tallest and “safest” skyscraper in China with his family.</p>
        <p class="open1"> The skyscraper itself houses several floors that function as its own society and despite the risks presented by Sawyer, who is the building’s head of security </p>
        <p><a href="#" class="btn btn-primary slide1">READ MORE</a></p>
        </div>
      </div>
    </div>
    <div class="col-xs-12 col-sm-4 well well-lg">
     <div class="thumbnail">
     <img src="./images/banner3.jpg" alt="first picture" class=" sky1" style="width: 100%;height: 100%;">
     <div class="caption">
    <h3>JURASSIC WORLD: FALLEN KINGDOM</h3>
    <p>Four years after the Jurassic World theme park was closed down, Owen and Claire return to Isla Nubar to save...</p>
    <p class="open2"> the dinosaurs when they learn that a once dormant volcano on the island is active and is threatening to extinguish all life there..</p>
    <p><a href="#" class="btn btn-primary slide2">READ MORE</a></p>
    </div>
    </div>
  </div>
 <div class="col-xs-12 col-sm-4 well well-lg">
   <div class="thumbnail">
  <img src="./images/banner4.jpg" alt="first picture" class=" sky1" style="width: 100%;height: 100%;">
  <div class="caption">
  <h3>ANT-MAN AND THE WASP</h3>
  <p>As Scott Lang balances being both a Super Hero and a father, Hope van Dyne and Dr. Hank Pym present an urgent </p>
  <p class="open3">new mission that finds the Ant-Man fighting alongside The Wasp to uncover secrets from their past.</p>
  <p><a href="#" class="btn btn-primary slide3">READ MORE</a></p>
  </div>
  </div>
</div>   
  </div>
</main> 

<footer style="color: white; background-color: #24262a">
    <div class="container" style="background-image:url(footer1.jpg) !important">
        <div class="row">
        <div class="hh col-sm-8">
                <h2><strong> Contact Us</strong></h2>
                <p> 2, Home assembly drive, <br/> Booust Avenue <br/> Nigeria.    </p>
                <p> Phone number : +234-8045678321</p>
                <p>  Email Add: info@booust.com</p>
        </div>
        <div class="hh col-sm-4">
               <p>&copy; A booust Initiative</p>
               <address>
                       Contact <a href="mailto:jsbabasanmi@gmail.com">Gbomaya</a> for any inquiries or opinions
               </address>
        </div>
        </div>
    </div>
</footer>
    
</div>

               
<script src="bootstrap files/scripts/jquery"></script>
<script src="bootstrap files/scripts/bootstrap.min.js"></script>

<script src="./js/script.js"></script> 
</body>
</html>