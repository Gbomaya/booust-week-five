<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>IKORODU Cinemas</title>
    <link href="https://fonts.googleapis.com/css?family=Merriweather|Ubuntu" rel="stylesheet">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="./bootstrap files/styles/bootstrap.min.css">
    <link rel="stylesheet" href="./css/index.css">
    <script src="js/jquery"></script>
    <script src="./bootstrap files/scripts/bootstrap.min.js"></script>
    <script src="./js/script.js"></script>
    <style> body {
        color: #e7344f;
        background-color: #93cddf;
      }</style>
</head>
<body>

<div class="container-fluid">
    <header>
        <div class=" pull-left">
            <a href="/"><img src="./images/banner.jpg" alt="Wallion" title="Wallion" style="height: 62px; width: 150px;" ></a>
        </div>
    <nav class="navbar navbar-expand-sm nucolor navbar-dark navbar-fixed-top left" style="background-color: #24262a">
    <a class="navbar-brand" href="weekfive.html">HOME</a>
     <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#collapsibleNavbar">
        <span class="navbar-toggler-icon"></span>
      </button>
    <div class="collapse navbar-collapse" id="collapsibleNavbar">
      <ul class="navbar-nav">
        <li class="nav-item">
        <a class="nav-link" href="about.html">ABOUT US</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="Contact.html">CONTACT US</a>
        </li>
       </ul>
    </div>  
    </nav>  
    </header>           
               
    <div class="jumbotron" style="background-image:url(images/missionimp2.jpg)">
        <div class="container" style="color: white">
        <h1>WELCOME TO IKORODU CINEMAS</h1>
        <p>WE SHOW YOUR FAVORITE MOVIES</p>
         <p><a href="#" class="btn btn-primary btn-lg text-center">EXPLORE OUR MOVIES</a></p>
      </div>
    </div>


    <div class="container-fluid middlecon" style="color: #24262a">
        <div class="container about"> 
          <h2>About US</h2> 
         <div class="row">
       <div class="">
       <h4 class="dhd"><strong>Welcome to Radar Film Rentals</strong></h4>
       <p> The only place to Watch and rent your movies in Ikorodu</p>
       <p> A little throwback to our history, we were established in 2016 by a movie enthusiast, Jeremiah<br/>
         Ikorodu Cinemas was established to cater for the movie needs and also form a community of movie enthusiasts to share their love for the big screen
       </p>
       </div>
       </div>
       <div class="row">
         <div class="">
           <h4 class="dhd"><strong>Rent your Old Movies in it's Original format</strong></h4>
           <p>  We have one of the largest catalogue of Movies in their original format, <br/>
             We value experience over quantity, we will help you relive the past by presenting <br/>
             the movie theatrics of Marlon Brandon, Elis Normalie and barbara streisand and Ronald Reagan</p>
           </div>
         </div>
           </div>
    </div>







 <footer style="color: white; background-color: #24262a">
        <div class="container" style="color: white; background-color: #24262a">
            <div class="row">
            <div class="hh col-sm-8">
                    <h2><strong> Contact Us</strong></h2>
                    <p> 2, Home assembly drive, <br/> Booust Avenue <br/> Nigeria.    </p>
                    <p> Phone number : +234-8045678321</p>
                    <p>  Email Add: info@booust.com</p>
            </div>
            <div class="hh col-sm-4">
                   <p>&copy; A booust Initiative</p>
                   <address>
                           Contact <a href="mailto:jsbabasanmi@gmail.com">Gbomaya</a> for any inquiries or opinions
                   </address>
            </div>
            </div>
        </div>
</footer>
        
</div>
    
                   
<script src="bootstrap files/scripts/jquery"></script>
<script src="bootstrap files/scripts/bootstrap.min.js"></script>

<script src="./js/script.js"></script> 
</body>
</html>