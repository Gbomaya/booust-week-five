



<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>IKORODU Cinemas</title>
    <link href="https://fonts.googleapis.com/css?family=Merriweather|Ubuntu" rel="stylesheet">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="./bootstrap files/styles/bootstrap.min.css">
    <link rel="stylesheet" href="./css/index.css">
    <script src="js/jquery"></script>
    <script src="./bootstrap files/scripts/bootstrap.min.js"></script>
    <!-- <script src="./js/script.js"></script> -->
    <style> body {
        color: #e7344f;
        background-color: #93cddf;
      }</style>
</head>
<body>

<div class="container-fluid">
    <header>
        <div class=" pull-left">
            <a href="/"><img src="./images/banner.jpg" alt="Wallion" title="Wallion" style="height: 62px; width: 150px;" ></a>
        </div>
    <nav class="navbar navbar-expand-sm nucolor navbar-dark navbar-fixed-top left" style="background-color: #24262a">
    <a class="navbar-brand" href="weekfive.html">HOME</a>
     <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#collapsibleNavbar">
        <span class="navbar-toggler-icon"></span>
      </button>
    <div class="collapse navbar-collapse" id="collapsibleNavbar">
      <ul class="navbar-nav">
        <li class="nav-item">
        <a class="nav-link" href="about.html">ABOUT US</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="Contact.html">CONTACT US</a>
        </li>
       </ul>
    </div>  
    </nav>  
    </header>           
               

   <div class="cont" style="height: 300%;color: rgb(75, 73, 73); font-family:'Segoe UI', Tahoma, Geneva, Verdana, sans-serif; padding-top: 80px;padding-bottom: 10px;">
     <div class="well well-lg conta"> Leave a Message</div>
     <p>Fill out all the required field to send a message</p>
  </div>

<!--USE A SIMPLE IF/ELSE STATEMENT -->

<?php  if (isset($_POST['email']) && ($_POST['fullName']) && ($_POST['comment']) ): 

 include 'mailer.php';
?>


<?php

//IMPORT YOUR CLASS HERE
include 'Classes/ContactUs.php';

//EXECUTE WITH THE CUSTOME STATIC FUNCTION
ContactUs::process($_POST);

?>

<div class="container" align="center">
   <h1>Thank You for contacting us</h1>
   <p> An email has been forwarded to you</p>
   <br>
   <a href="Contact.php"> Contact Us </a>
</div>

<?php else:  ?>

<div>   
  <form action="index.php" method="POST" style="height:100%; color: #24262a; padding-bottom: 20px; ">
    <div class="form-group">
      <label for="email">Email address:</label>
      <input type="email" class="form-control" placeholder="Enter email" id="email" name='email'>
      <p id="email_error" style="font-size: smaller"></p>
    </div>
    <div class="form-group">
      <label for="fullName">Full Name:</label>
      <input type="fullName" class="form-control"placeholder="Enter Full Name" id="fullName" name='fullName'>
      <p id="fullName_error" style="font-size:smaller"></p>
    </div>
    <div class="form-group">
        <label for="comment">Comment:</label>
        <textarea class="form-control" rows="8" placeholder="Your Comments" id="comment" name='comment'></textarea>
        <p id="message_error" style="font-size: smaller"></p>
    </div>
    <input type="hidden" name="form_submitted" value="1" />
    <button type="submit" id="submitForm" class="btn btn-default">Submit</button>
  </form>
</div>

<?php endif; ?>



 <footer style="color: white; background-color: #24262a">
        <div class="container">
            <div class="row">
            <div class="hh col-sm-8">
                    <h2><strong> Contact Us</strong></h2>
                    <p> 2, Home assembly drive, <br/> Booust Avenue <br/> Nigeria.    </p>
                    <p> Phone number : +234-8045678321</p>
                    <p>  Email Add: info@booust.com</p>
            </div>
            <div class="hh col-sm-4">
                   <p>&copy; A booust Initiative</p>
                   <address>
                           Contact <a href="mailto:jsbabasanmi@gmail.com">Gbomaya</a> for any inquiries or opinions
                   </address>
            </div>
            </div>
        </div>
</footer>
        
</div>
    
                   
<script src="bootstrap files/scripts/jquery"></script>
<script src="bootstrap files/scripts/bootstrap.min.js"></script>

<!-- <script src="./js/script.js" defer></script> -->
</body>
</html>